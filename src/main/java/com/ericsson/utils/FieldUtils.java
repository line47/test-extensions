package com.ericsson.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class FieldUtils {

    private static final String MODIFIERS_FIELD_NAME_IN_FIELD_CLASS = "modifiers";

    private FieldUtils() {
    }

    public static List<Field> getAllFields(final Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(FieldUtils::isNotSynthetic)
                .filter(FieldUtils::isNotStatic)
                .collect(Collectors.toList());
    }

    public static List<Field> getAllFieldsExcluding(final Class<?> clazz, final List<String> excludedFields) {
        return getAllFields(clazz).stream()
                .filter(field -> doesNotContain(field, excludedFields))
                .collect(Collectors.toList());
    }


    public static List<String> getAllFieldNames(final Class<?> clazz) {
        return getAllFields(clazz).stream()
                .map(Field::getName)
                .collect(Collectors.toList());
    }

    public static Object getValue(final Object targetObject, final Field field) throws IllegalAccessException, NoSuchFieldException {
        makeModifiable(field);
        return field.get(targetObject);
    }

    public static void setValue(final Object targetObject, final Field field, final Object value) throws IllegalAccessException, NoSuchFieldException {
        makeModifiable(field);
        field.set(targetObject, value);
    }

    public static List<Field> getFields(final Class<?> testedClass, final Predicate<String> predicate) {
        return getAllFields(testedClass).stream()
                .filter(eachField -> predicate.test(eachField.getName()))
                .collect(Collectors.toList());
    }

    public static boolean isFinal(final Field field) {
        final int fieldModifiers = field.getModifiers();
        return Modifier.isFinal(fieldModifiers);
    }

    public static List<Field> getSpecifiedFields(final Class<?> clazz, final List<String> names) {
        return names.stream()
                .map(name -> getField(clazz, name))
                .collect(Collectors.toList());
    }

    private static void makeModifiable(final Field field) throws NoSuchFieldException, IllegalAccessException {
        final Class<? extends Field> clazz = field.getClass();
        field.setAccessible(true);
        final Field modifierField = clazz.getDeclaredField(MODIFIERS_FIELD_NAME_IN_FIELD_CLASS);
        modifierField.setAccessible(true);

        final int modifiers = field.getModifiers() & ~Modifier.FINAL;
        modifierField.setInt(field, modifiers);
    }

    private static boolean excludeEmptySet(final List<Field> fields) {
        return !fields.isEmpty();
    }

    private static boolean isNotSynthetic(final Field field) {
        return !field.isSynthetic();
    }

    private static boolean isNotStatic(final Field field) {
        return !Modifier.isStatic(field.getModifiers());
    }

    private static boolean doesNotContain(final Field field, final List<String> excludedFields) {
        return !excludedFields.contains(field.getName());
    }

    private static Field getField(final Class<?> clazz, final String name) {
        try {
            return clazz.getDeclaredField(name);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return null;
    }
}
