package com.ericsson.utils;

import java.util.Optional;

public final class ClassLoader {

    private ClassLoader() {
    }

    public static Optional<Class<?>> loadClass(final String qualifiedClassName) {
        Class<?> aClass = null;
        try {
            aClass = Class.forName(qualifiedClassName);
        } catch (ClassNotFoundException e) {
            //ignored
        }

        return Optional.ofNullable(aClass);
    }
}
