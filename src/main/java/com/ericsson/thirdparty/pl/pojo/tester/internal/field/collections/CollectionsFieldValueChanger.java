package com.ericsson.thirdparty.pl.pojo.tester.internal.field.collections;


import com.ericsson.thirdparty.pl.pojo.tester.internal.field.AbstractFieldValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.collections.collection.AbstractCollectionFieldValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.collections.iterators.AbstractIteratorsFieldValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.collections.map.AbstractMapFieldValueChanger;

public final class CollectionsFieldValueChanger {

    public static final AbstractFieldValueChanger INSTANCE = new ArrayValueChanger()
            .attachNext(new StreamValueChanger())
            .attachNext(AbstractCollectionFieldValueChanger.INSTANCE)
            .attachNext(AbstractMapFieldValueChanger.INSTANCE)
            .attachNext(AbstractIteratorsFieldValueChanger.INSTANCE);

    private CollectionsFieldValueChanger() {}
}
