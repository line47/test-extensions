package com.ericsson.thirdparty.pl.pojo.tester.internal.field;


import com.ericsson.thirdparty.pl.pojo.tester.internal.field.collections.CollectionsFieldValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.date.DefaultDateAndTimeFieldValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.math.BigDecimalValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.math.BigIntegerValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.primitive.AbstractPrimitiveValueChanger;


public final class DefaultFieldValueChanger {

    public static final AbstractFieldValueChanger INSTANCE = new EnumValueChanger()
            .attachNext(AbstractPrimitiveValueChanger.INSTANCE)
            .attachNext(CollectionsFieldValueChanger.INSTANCE)
            .attachNext(DefaultDateAndTimeFieldValueChanger.INSTANCE)
            .attachNext(new StringValueChanger())
            .attachNext(new UUIDValueChanger())
            .attachNext(new BigDecimalValueChanger())
            .attachNext(new BigIntegerValueChanger());

    private DefaultFieldValueChanger() {
    }
}
