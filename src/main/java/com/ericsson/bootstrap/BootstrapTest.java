package com.ericsson.bootstrap;

/**
 * Created by Talal Ahmed on 29/05/2018
 */
public interface BootstrapTest {
    String getRootPackage();
}
