package com.ericsson.junit.runner;

import com.ericsson.thirdparty.pl.pojo.tester.internal.assertion.TestAssertions;
import com.ericsson.thirdparty.pl.pojo.tester.internal.field.DefaultFieldValueChanger;
import com.ericsson.thirdparty.pl.pojo.tester.internal.instantiator.ObjectGenerator;
import com.ericsson.thirdparty.pl.pojo.tester.internal.utils.ThoroughFieldPermutator;
import com.ericsson.utils.FieldUtils;
import com.ericsson.utils.MethodUtils;
import com.ericsson.utils.ReflectionUtils;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;

/**
 * Created by Talal Ahmed on 24/05/2018
 */
public class TestRunner extends Runner {

    private Class testClass;
    private ObjectGenerator objectGenerator;
    private final TestAssertions testAssertions = new TestAssertions();

    public TestRunner(Class testClass) {
        super();
        this.testClass = testClass;
    }

    @Override
    public Description getDescription() {
        return Description.createTestDescription(testClass, "Test runner with getter/setter/equals coverage");
    }

    @Override
    public void run(RunNotifier notifier) {
        System.out.println("\nRunning " + testClass);
        try {
            objectGenerator = new ObjectGenerator(DefaultFieldValueChanger.INSTANCE,
                    new ArrayListValuedHashMap<>(),
                    new ThoroughFieldPermutator());

            Object testClassInstance = testClass.newInstance();

            // All methods with @Test annotations
            testAnnotatedMethods(notifier, testClassInstance);

            // Find source classes from given test class to retrieve member properties
            Optional<Class<?>> srcClassFromTestRef = ReflectionUtils.getSourceClassFromTestRef(testClass);

            if (srcClassFromTestRef.isPresent()) {
                Object srcObject = objectGenerator.createNewInstance(srcClassFromTestRef.get());
                testGetterSetter(srcObject, srcClassFromTestRef.get());
                testEquality(srcObject);
            }

        } catch (InstantiationException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    private void testAnnotatedMethods(RunNotifier notifier, Object testInstance) {
        try {
            for (Method method : testClass.getMethods()) {
                if (method.isAnnotationPresent(Test.class)) {
                    notifier.fireTestStarted(Description.createTestDescription(testClass, method.getName()));
                    method.invoke(testInstance);
                    notifier.fireTestFinished(Description.createTestDescription(testClass, method.getName()));
                }
            }

        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void testGetterSetter(Object srcObject, Class<?> srcClazz) {
        List<Field> allFields = FieldUtils.getAllFields(srcClazz);
        allFields.forEach(f -> System.out.println("Found field: " + f.getName()));

        for (Field field : allFields) {
            Optional<Method> setterMethod = MethodUtils.findSetterFor(srcClazz, field);
            setterMethod.ifPresent(method -> {
                testSetter(srcObject, field, method);
            });

            Optional<Method> getterMethod = MethodUtils.findGetterFor(srcClazz, field);
            getterMethod.ifPresent(method -> {
                testGetter(srcObject, field, method);
            });
        }

    }

    private void testGetter(Object classInstance, Field field, Method method) {
        System.out.println("Testing getter: " + method);

        try {
            testAssertions.assertThatGetMethodFor(classInstance)
                    .willGetValueFromField(method, field);
        } catch (Exception e) {
            //ignored
        }
    }

    private void testSetter(Object classInstance, Field field, Method method) {
        System.out.println("Testing setter: " + method);

        final Object newValue = objectGenerator.createNewInstance(field.getType());
        try {
            testAssertions.assertThatSetMethodFor(classInstance)
                    .willSetValueOnField(method, field, newValue);
        } catch (Exception e) {
            //ignored
        }
    }

    private void testEquality(Object classInstance) {
        System.out.println("Testing equality: public boolean " + classInstance.getClass().getName() + ".equals(Object)");

        try {
            testAssertions.assertThatEqualsMethodFor(classInstance)
                    .isConsistent();
        } catch (Exception e) {
            //ignored
        }
    }
}