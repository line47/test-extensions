package com.ericsson.bootstrap;

import com.ericsson.junit.runner.BootstrapRunner;
import org.junit.runner.RunWith;

/**
 * Created by Talal Ahmed on 24/05/2018
 */
@RunWith(BootstrapRunner.class)
public class BootstrapBaseTests implements BootstrapTest {

    @Override
    public String getRootPackage() {
        return "com.ericsson.samples";
    }
}
