package com.ericsson.bootstrap;

import com.ericsson.junit.runner.BootstrapSuiteRunner;
import com.ericsson.samples.TestTriangle;
import com.ericsson.samples.TriangleTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Talal Ahmed on 24/05/2018
 */
@RunWith(BootstrapSuiteRunner.class)
@Suite.SuiteClasses({TriangleTest.class, TestTriangle.class})
public class BootstrapSuiteTests {

}
