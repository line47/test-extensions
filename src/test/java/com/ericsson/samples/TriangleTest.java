package com.ericsson.samples;

import com.ericsson.junit.runner.BootstrapSuiteRunner;
import com.ericsson.junit.runner.TestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.ericsson.samples.Triangle.Type.*;
import static org.junit.Assert.assertEquals;

@RunWith(TestRunner.class)
public class TriangleTest {

    @Test
    public void test1() {
        assertEquals(Triangle.classify(0, 1301, 1), INVALID);
    }

    @Test
    public void test2() {
        assertEquals(Triangle.classify(1108, 1, 1), INVALID);
    }

    @Test
    public void test3() {
        assertEquals(Triangle.classify(1, 0, -665), INVALID);
    }

    @Test
    public void test4() {
        assertEquals(Triangle.classify(1, 1, 0), INVALID);
    }

    @Test
    public void test5() {
        assertEquals(Triangle.classify(582, 582, 582), EQUILATERAL);
    }

    @Test
    public void test6() {
        assertEquals(Triangle.classify(1, 1088, 15), INVALID);
    }

    @Test
    public void test7() {
        assertEquals(Triangle.classify(1, 2, 450), INVALID);
    }

    @Test
    public void test8() {
        assertEquals(Triangle.classify(1663, 1088, 823), SCALENE);
    }

    public void test9() {
        assertEquals(Triangle.classify(1187, 1146, 1), INVALID);
    }

    @Test
    public void test10() {
        assertEquals(Triangle.classify(1640, 1640, 1956), ISOSCELES);
    }

    @Test
    public void test11() {
        assertEquals(Triangle.classify(784, 784, 1956), INVALID);
    }

    @Test
    public void test12() {
        assertEquals(Triangle.classify(1, 450, 1), INVALID);
    }

    @Test
    public void test13() {
        assertEquals(Triangle.classify(1146, 1, 1146), ISOSCELES);
    }

    @Test
    public void test14() {
        assertEquals(Triangle.classify(1640, 1956, 1956), ISOSCELES);
    }

    @Test
    public void test15() {
        assertEquals(Triangle.classify(-1, 1, 1), INVALID);
    }

    @Test
    public void test16() {
        assertEquals(Triangle.classify(1, -1, 1), INVALID);
    }

    @Test
    public void test17() {
        assertEquals(Triangle.classify(1, 2, 3), INVALID);
    }

    @Test
    public void test18() {
        assertEquals(Triangle.classify(2, 3, 1), INVALID);
    }

    @Test
    public void test19() {
        assertEquals(Triangle.classify(3, 1, 2), INVALID);
    }

    @Test
    public void test20() {
        assertEquals(Triangle.classify(1, 1, 2), INVALID);
    }

    @Test
    public void test21() {
        assertEquals(Triangle.classify(1, 2, 1), INVALID);
    }

    @Test
    public void test22() {
        assertEquals(Triangle.classify(2, 1, 1), INVALID);
    }

    @Test
    public void test23() {
        assertEquals(Triangle.classify(1, 1, 1), EQUILATERAL);
    }

    @Test
    public void test24() {
        assertEquals(Triangle.classify(0, 1, 1), INVALID);
    }

    @Test
    public void test25() {
        assertEquals(Triangle.classify(1, 0, 1), INVALID);
    }

    @Test
    public void test26() {
        assertEquals(Triangle.classify(1, 2, -1), INVALID);
    }

    @Test
    public void test27() {
        assertEquals(Triangle.classify(1, 1, -1), INVALID);
    }

    @Test
    public void test28() {
        assertEquals(Triangle.classify(0, 0, 0), INVALID);
    }

    @Test
    public void test29() {
        assertEquals(Triangle.classify(3, 2, 5), INVALID);
    }

    @Test
    public void test30() {
        assertEquals(Triangle.classify(5, 9, 2), INVALID);
    }

    @Test
    public void test31() {
        assertEquals(Triangle.classify(7, 4, 3), INVALID);
    }

    @Test
    public void test32() {
        assertEquals(Triangle.classify(3, 8, 3), INVALID);
    }

    @Test
    public void test33() {
        assertEquals(Triangle.classify(7, 3, 3), INVALID);
    }
}
